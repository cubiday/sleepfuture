// SPDX-License-Identifier: MIT
pragma solidity ^0.8.0;

import "./deps/Ownable.sol";
import "./deps/IERC20.sol";

contract VestingContract {

    IERC20 internal token;

    // start cliff time is deployment time
    uint256 internal amount;
    uint256 internal startReleaseDate;
    uint256 internal distributed;
    uint256 internal releasePeriod;
    uint256 internal distributedTime;
    uint256 internal percentage;
    address internal beneficiary;

    constructor (IERC20 _token, uint256 _amount, uint256 _distributed, address _beneficiary, uint256 _percentage, uint256 _cliffTime, uint256 _releasePeriod) {
        startReleaseDate = block.timestamp + _cliffTime;
        releasePeriod = _releasePeriod;

        token = _token;
        amount = _amount;
        distributed = _distributed;
        beneficiary = _beneficiary;
        percentage = _percentage;
    }

    function release() public {
        // fully release
        require(amount > distributed, "Vesting is fully released") ;
        require(startReleaseDate + distributedTime*releasePeriod < block.timestamp, "Token is still in lock");

        uint256 periodReleaseToken = percentage*amount/10000;
        uint256 releaseToken = (amount - distributed) > periodReleaseToken ? periodReleaseToken: (amount - distributed);

        distributed = distributed + releaseToken;
        distributedTime++;

        token.transfer(beneficiary, releaseToken);
    }

    function vestingInfo() public view returns (
        IERC20 _token, address _beneficiary, uint256 _amount, uint256 _distributed, 
        uint256 _startReleaseDate, uint256 _releasePeriod, uint256 _distributedTime, uint256 _percentage) {
        
        return (token, beneficiary, amount, distributed, startReleaseDate, releasePeriod, distributedTime, percentage);
    }
}
