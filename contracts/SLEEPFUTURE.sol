// SPDX-License-Identifier: MIT
pragma solidity ^0.8.0;

import "./deps/Ownable.sol";
import "./deps/ERC20Burnable.sol";
import "./deps/ERC20Pausable.sol";
import "./VestingContract.sol";


contract SLEEPFUTURE is ERC20Pausable, ERC20Burnable, Ownable {
    uint256 constant TOTAL_SUPPLY = 1*(10**9)*(10**18); //1 billion tokens;

    uint256 internal token4Seed = 4*TOTAL_SUPPLY/100; //4%
    uint256 internal token4PrivateSale = 3*TOTAL_SUPPLY/100; //3%
    uint256 internal token4PreSale = 2*TOTAL_SUPPLY/100; //2%
    uint256 internal token4PublicSale = 1*TOTAL_SUPPLY/100; //1%
    uint256 internal token4Team = 16*TOTAL_SUPPLY/100; // 16%
    uint256 internal token4Rewards = 30*TOTAL_SUPPLY/100; // 30%
    uint256 internal token4Foundation = 20*TOTAL_SUPPLY/100; // 20%
    uint256 internal token4Marketing = 12*TOTAL_SUPPLY/100; // 12%
    uint256 internal token4Liquidity = 9*TOTAL_SUPPLY/100; // 9%
    uint256 internal token4Reserve = 3*TOTAL_SUPPLY/100; // 3%

    address[] internal vestingLists;

    //Total 100%

    constructor () ERC20 ("SLEEPFUTURE", "SLEEPEE"){
        _genesisDistributeAndLock();
        //_genesisDistributeAndLockTestnet();
    }

    function pause() public onlyOwner {
        _pause();
    }

    function unpause() public onlyOwner {
        _unpause();
    }

    function getVestings() public view returns (address[] memory vestings) {
        return vestingLists;
    }

    /**
     * @dev See {ERC20-_beforeTokenTransfer}.
     *
     * Requirements:
     *
     * - the contract must not be paused.
     */
    function _beforeTokenTransfer(
        address from,
        address to,
        uint256 amount
    ) internal virtual override (ERC20, ERC20Pausable) {
        super._beforeTokenTransfer(from, to, amount);

        require(!paused(), "ERC20Pausable: token transfer while paused");
    }

    
     /**
     * distribute token for team, marketing, foundation, ...
     */
    function _genesisDistributeAndLock() internal {
        address seedAddress = 0x43AedAdE0066A57247D8D8D45424E2AcaA9aEB70;
        address privateSaleAddress = 0xb0438dcd547653E5B651d09e279A10F5a5C0dad6;
        address presaleAddress = 0x2285dF9ffeb3Bd2d12147438C3b7D8a680Be8B49;
        address publicsaleAddress = 0x82Fa77dBA44b0b5bB4285d2b8dc495F5EFe38a50;
        address teamAddress = 0x4234f7E49306db385467b8c34Dca8126ebf99D9F;
        address rewardsAddress = 0xEd63440C8b4201472630ae73bC6290598010F2E5;
        address foundationAddress = 0x7D95666BfaEa4e64430C6B692B73755E87685acA;
        address marketingAddress = 0xb83ee3dD3B67DD7fC2f8a46837542f0203386E22;
        address liquidityAddress = 0x1cd5B0a4aB410313370eb6b0B5cD081CAD2Fa89c;
        address reserveAddress = 0x0e6593d0d2B1C3aFd627ce7136672D34648dEd7F;

        // distribute for ecosystem and stacking
        _distributeAndLockToken(500, token4Seed, seedAddress, 791, 90 days, 30 days);
        _distributeAndLockToken(500, token4PrivateSale, privateSaleAddress, 1055, 90 days, 30 days);
        _distributeAndLockToken(0, token4PreSale, presaleAddress, 2000, 10 days, 30 days);
        _distributeAndLockToken(2000, token4PublicSale, publicsaleAddress, 1333, 30 days, 30 days);
        _distributeAndLockToken(0, token4Team, teamAddress, 500, 365 days, 30 days);
        _distributeAndLockToken(0, token4Rewards, rewardsAddress, 500, 30 days, 30 days);
        _distributeAndLockToken(100, token4Foundation, foundationAddress, 450, 90 days, 30 days);
        _distributeAndLockToken(0, token4Marketing, marketingAddress, 300, 15 days, 30 days);
        _distributeAndLockToken(100, token4Liquidity, liquidityAddress, 500, 30 days, 30 days);
        _distributeAndLockToken(100, token4Reserve, reserveAddress, 500, 90 days, 30 days);
    }

      /**
     * distribute token for team, marketing, foundation, ...
     */
    function _genesisDistributeAndLockTestnet() internal {
        address seedAddress = 0x43AedAdE0066A57247D8D8D45424E2AcaA9aEB70;
        address privateSaleAddress = 0xb0438dcd547653E5B651d09e279A10F5a5C0dad6;
        address presaleAddress = 0x2285dF9ffeb3Bd2d12147438C3b7D8a680Be8B49;
        address publicsaleAddress = 0x82Fa77dBA44b0b5bB4285d2b8dc495F5EFe38a50;
        address teamAddress = 0x4234f7E49306db385467b8c34Dca8126ebf99D9F;
        address rewardsAddress = 0x1066A4AaBa6f3F4d8e2172090d28f50dbeEc0809;// test account
        address foundationAddress = 0x7D95666BfaEa4e64430C6B692B73755E87685acA;
        address marketingAddress = 0xb83ee3dD3B67DD7fC2f8a46837542f0203386E22;
        address liquidityAddress = 0x1cd5B0a4aB410313370eb6b0B5cD081CAD2Fa89c;
        address reserveAddress = 0x0e6593d0d2B1C3aFd627ce7136672D34648dEd7F;
        
        // distribute for ecosystem and stacking
        _distributeAndLockToken(500, token4Seed, seedAddress, 791, 1 minutes, 1 minutes);
        _distributeAndLockToken(500, token4PrivateSale, privateSaleAddress, 1550, 1 minutes, 1 minutes);
        _distributeAndLockToken(0, token4PreSale, presaleAddress, 2000, 1 minutes, 1 minutes);
        _distributeAndLockToken(2000, token4PublicSale, publicsaleAddress, 1333, 1 minutes, 1 minutes);
        _distributeAndLockToken(0, token4Team, teamAddress, 500, 1 minutes, 1 minutes);
        _distributeAndLockToken(0, token4Rewards, rewardsAddress, 500, 1 minutes, 1 minutes);
        _distributeAndLockToken(100, token4Foundation, foundationAddress, 450, 1 minutes, 1 minutes);
        _distributeAndLockToken(0, token4Marketing, marketingAddress, 300, 1 minutes, 1 minutes);
        _distributeAndLockToken(100, token4Liquidity, liquidityAddress, 500, 1 minutes, 1 minutes);
        _distributeAndLockToken(100, token4Reserve, reserveAddress, 500, 1 minutes,1 minutes);
    }

    function _distributeAndLockToken(uint256 _tgePercent, uint256 _totalVesting, address _beneficiary, uint256 _periodPecentage, uint256 _cliffTime, uint256 _releasePeriod) internal {
        uint256 tgeToken = _tgePercent*_totalVesting/10000;

        // (IERC20 _token, uint _amount, uint _distributed, address _beneficiary, uint _percentage, uint _cliffTime, uint _releasePeriod)
        VestingContract vesting = new VestingContract(
            IERC20(this),
            _totalVesting,
            tgeToken,
            _beneficiary,
            _periodPecentage,
            _cliffTime,
            _releasePeriod
        );

        vestingLists.push(address(vesting));

        _mint(address(vesting), _totalVesting - tgeToken);
        if(tgeToken > 0) _mint(_beneficiary, tgeToken);
    }
}