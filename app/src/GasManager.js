"use strict"
// const Client = require('node-rest-client').Client;
const axios = require('axios');
const { inherits, promisify } = require('util');
const BigNumber = require('bignumber.js');
const Util = require('./Util');
const Constants = require('./Constants');

const options ={  
    mimetypes:{
        json:["application/json","application/json; charset=utf-8"]
    } 
};

const GasManager = function (web3) {
	// set default gas and gasPrice value
    this.container = void 0;
    this.web3 = web3;
	this.gas = 300000;
	// this.gasPrice = this.web3.utils.toWei("10","gwei");
	this.networkGasPrice = 0;
	this.fastGasPrice = 0;
    this.fastestGasPrice = 0;
    this.average = 0;
    this.slow = 0;
    this.FAST_GAS_FACTOR = this.web3.utils.toWei("4","gwei");;
    this.GAS_LIMIT = new BigNumber(""+this.web3.utils.toWei("400","gwei"));
	// this.gwei = web3.utils.toWei("1","gwei");

	this.init();
}

GasManager.prototype.init = function () {
    this.updateGasPrice();
    this.internalId = setInterval(this.updateGasPrice.bind(this), 10*60*1000);
}

GasManager.prototype.getGasPrice = function () {
    return this.networkGasPrice;
}

GasManager.prototype.updateGasPrice = async function (option = Constants.GAS_FAST) {
    const _networkGasPrice = await this.web3.eth.getGasPrice();
    const gasRecommend = new BigNumber(""+_networkGasPrice).plus(new BigNumber(this.FAST_GAS_FACTOR));
    this.networkGasPrice = gasRecommend;
    this.average = new BigNumber(""+_networkGasPrice).multipliedBy(0.95);
    this.slow = this.average.multipliedBy(0.85);
    
    this.log(Util.sprintf("Network gas price: {0}, slow gas {1}, avg {2}, fast {3}", 
            Util.weiToGwei(_networkGasPrice), 
            Util.weiToGwei(this.slow), 
            Util.weiToGwei(this.average), 
            Util.weiToGwei(this.networkGasPrice))
            );
    try {
        let url = "https://ethgasstation.info/json/ethgasAPI.json";

        let result = await axios.get(url);
        const gasPrice = result.data;

        if(isNaN(gasPrice.fast) || isNaN(gasPrice.fastest)) {
            console.log("Invalid gas price");
            return;
        }
        
        const fast = gasPrice.fast/10;
        const fastest = gasPrice.fastest/10;
        const avg = gasPrice.average/10;
        const slow = gasPrice.safeLow/10;

        const fastOracle = this.web3.utils.toWei(fast.toFixed(0).toString(),"gwei");
        const fastestOracle = this.web3.utils.toWei(fastest.toFixed(0).toString(), "gwei");
        const avgOracle = this.web3.utils.toWei(avg.toFixed(0).toString(), "gwei");
        const slowOracle = this.web3.utils.toWei(slow.toFixed(0).toString(), "gwei");

        this.fastGasPrice = fastOracle;
        this.fastestGasPrice = fastestOracle;

        //gasRecommend = Util.weiToGwei(gasRecommend);


        // calculate recommend gas
        if(fastOracle < gasRecommend && gasRecommend < fastestOracle) {
            this.networkGasPrice = gasRecommend;
        }
        if(gasRecommend > fastestOracle) {
            this.networkGasPrice = fastestOracle
        }

        // if(new BigNumber(this.average).gt(avgOracle)) this.average = avgOracle;
        // if(new BigNumber(this.slow).gt(slowOracle)) this.slow = slowOracle;

        this.log(Util.sprintf("fastOracle: {0}, fastestOracle {1}, avg {2}, slow {3}, gasRecommend {4}, gasLimit {5}", 
            Util.weiToGwei(fastOracle), 
            Util.weiToGwei(fastestOracle),
            Util.weiToGwei(avgOracle),
            Util.weiToGwei(slowOracle),
            Util.weiToGwei(gasRecommend), 
            Util.weiToGwei(this.GAS_LIMIT)
            )
        );
        
        // rouding gasRecommend
        const finalGas = new BigNumber(this.GAS_LIMIT).comparedTo(new BigNumber(gasRecommend))
        this.networkGasPrice = finalGas > 0? gasRecommend: this.GAS_LIMIT;

        this.log(Util.sprintf("Final applied gas price after combining with ethgasstation: {0}, slow {1}, avg {2}, fast {3}", 
            Util.weiToGwei(_networkGasPrice), 
            Util.weiToGwei(this.slow), 
            Util.weiToGwei(this.average), 
            Util.weiToGwei(this.networkGasPrice))
        );
    } catch (e) {
        this.log("Error", e);
    }

    return this.getGasPriceOption(option);
    // return this.networkGasPrice;
}

GasManager.prototype.getRecommendGasPrice = async function () {
    const _networkGasPrice = await this.web3.eth.getGasPrice();
	// if network
	if(this.networkGasPrice == 0) {
        this.networkGasPrice = (_networkGasPrice * this.FAST_GAS_FACTOR).toFixed(0);
    }
    
    if(_networkGasPrice < this.networkGasPrice) {
        this.networkGasPrice = _networkGasPrice;
    }

	return this.networkGasPrice;
}

GasManager.prototype.getGasPriceInWei = function() {
    return Util.weiToGwei(this.networkGasPrice);
}


GasManager.prototype.getGasPriceOption = function (option = Constants.GAS_FAST) {
    if(this.slow.gt(this.GAS_LIMIT)) this.slow = this.GAS_LIMIT;
    if(this.average.gt(this.GAS_LIMIT)) this.average = this.GAS_LIMIT;
    if(this.networkGasPrice.gt(this.GAS_LIMIT)) this.networkGasPrice = this.GAS_LIMIT;


    if(option == Constants.GAS_SLOW) return this.slow;
    else if (option == Constants.GAS_AVERAGE) return this.average;
    else if (option == Constants.GAS_FAST) return this.networkGasPrice;
    else return this.slow;
}

GasManager.prototype.log = function () {
    let date = new Date().toISOString();
    let args = Array.from(arguments);
    let msg = args.shift();
    msg = Util.sprintf("{0} [{1}] - {2}", date, this.constructor.name, msg);
    args.unshift(msg);
    console.log.apply(console, args);
}

module.exports = GasManager;
