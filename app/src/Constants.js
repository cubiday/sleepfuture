const Constants = {
    GAS_SLOW: 1,
    GAS_AVERAGE: 2,
    GAS_FAST: 3
}

module.exports = Constants;