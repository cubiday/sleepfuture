"use strict";

const axios = require('axios');
const Web3 = require('web3');
const inherits = require('util').inherits;
const Base = require("./Base");
const GasManager = require('./GasManager');
const { tokenABI, vestingABI } = require('../abi');

let LOG = void 0;

const VestingManager = function() {
    this.RELEASE_INTERVAL = process.env.RELEASE_INTERVAL || 5 ; // 5 minutes
    this.TIME_WINDOW = process.env.TIME_WINDOW || 5 ; // 5 minutes

    this.web3 = new Web3(process.env.RPC_PROVIDER);
    this.sender = this.web3.eth.accounts.privateKeyToAccount(process.env.SENDER_PK);
    this.gasManager = new GasManager(this.web3);
    this.tokenContract = new this.web3.eth.Contract(tokenABI, process.env.TOKEN_CONTRACT_ADDRESS);
    LOG = this;
}

inherits(VestingManager, Base);

VestingManager.prototype.start = async function() {
    setInterval(this.releaseToken.bind(this), this.RELEASE_INTERVAL * 60 * 1000);
    
    // LOG.info(this.vestings);
    LOG.info(`Started release token service...`);
    LOG.info(`Token ${process.env.TOKEN_CONTRACT_ADDRESS}, Interval ${this.RELEASE_INTERVAL}, RPC: ${process.env.RPC_PROVIDER}`);

    this.releaseToken();
}

VestingManager.prototype.resync = async function() {
    let vestingContracts = await this.tokenContract.methods.getVestings().call();
    
    for(let idx = 0; idx < vestingContracts.length; idx++) {
        let vestingContract = this._getVestingContract(vestingContracts[idx]);
        let vestingInfo = await vestingContract.methods.vestingInfo().call();
        vestingInfo = {...vestingInfo, address: vestingContracts[idx]};
        this.vestings.push(vestingInfo);
    }
}

VestingManager.prototype.releaseToken = async function() {
    if(!this.sender) {
        LOG.info(`ERROR: There is no sender is setup yet, please setup sender`);
        return;
    }

    try {
        let vestingContracts = await this.tokenContract.methods.getVestings().call();
        if(!vestingContracts || vestingContracts.length == 0) {
            LOG.info(`There is no vesting info, please check token contract address, IGNORE`);
            return;
        }

        const currentBlockNumber = await this.web3.eth.getBlockNumber();
        let { timestamp } = await this.web3.eth.getBlock(currentBlockNumber);
        
        for(let idx = 0; idx < vestingContracts.length; idx++) {
            let vestingContract = this._getVestingContract(vestingContracts[idx]);
            let vestingInfo = await vestingContract.methods.vestingInfo().call();
            vestingInfo = {...vestingInfo, address: vestingContracts[idx]};

            let { address, _token,_beneficiary, _amount, _distributed, _distributedTime, _startReleaseDate, _releasePeriod } = vestingInfo;

            _startReleaseDate = parseInt(_startReleaseDate);
            _releasePeriod = parseInt(_releasePeriod);
            _distributedTime = parseInt(_distributedTime);

            if(_amount == _distributed) {
                LOG.info(`Vesting ${address} is already fully unlocked, IGNORE`);
                continue;
            }

            LOG.info(`${address}, ${_startReleaseDate}, ${_distributedTime}, ${_releasePeriod}, ${timestamp}`);
            LOG.info(`Checking vesting ${address}, is able to release => ${_startReleaseDate + _distributedTime * _releasePeriod < timestamp + this.TIME_WINDOW * 60}`)

            if(_startReleaseDate + _distributedTime * _releasePeriod < timestamp + this.TIME_WINDOW * 60 && _amount != _distributed) {
                LOG.info(`Releasing vesting`, JSON.stringify(vestingInfo));

                let vestingContract = this._getVestingContract(address);

                let payload = await vestingContract.methods.release().encodeABI();
                let gasPrice = this.gasManager.getGasPriceOption().toFixed(0);
                let nonce = await this.web3.eth.getTransactionCount(this.sender.address);

                let txt = {
                    from: this.sender.address,
                    to: address,
                    gas: 200000,
                    gasPrice: gasPrice,
                    nonce,
                    data: payload
                }

                let signedTx = await this.sender.signTransaction(txt);
                let txReceipt = await this.web3.eth.sendSignedTransaction(signedTx.rawTransaction);

                LOG.info('Released TxHash', txReceipt);
            }
        }
    } catch (error) {
        LOG.info('ERROR in processing vesting info', error);
    }

}

VestingManager.prototype._getVestingContract = function(address) {
    return new this.web3.eth.Contract(vestingABI, address);
}

module.exports = VestingManager;