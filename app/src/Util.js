"use strict";

const _ = require('lodash');
const BigNumber = require("bignumber.js");

const DECIMALS = 18;
const BASE_DECIMALS = new BigNumber(10).exponentiatedBy(DECIMALS);
const Util = {};
Util.sprintf = function () {
    for (var a = arguments[0], b = 1; b < arguments.length; b++){
    	a = a.replaceAll("{" + (b - 1) + "}", arguments[b]);
    }
    return a
};

Util.stringEqual = function(str1, str2) {
    return _.toLower(str1) == _.toLower(str2);
}

Util.isEmpty = function (str) {
    return !str || str.trim() == '';
}

Util.numberToWei = function (number) {
    return BASE_DECIMALS.multipliedBy(number);
}

Util.weiToGwei = function (wei) {
    return new BigNumber(wei).dividedBy(new BigNumber("1000000000"));
}

String.prototype.replaceAll = function(
	strTarget, // The substring you want to replace
	strSubString // The string you want to replace in.
	){
	var strText = this;
	var intIndexOfMatch = strText.indexOf( strTarget );
	 
	// Keep looping while an instance of the target string
	// still exists in the string.
	while (intIndexOfMatch != -1){
	// Relace out the current instance.
	strText = strText.replace( strTarget, strSubString )
	 
	// Get the index of any next matching substring.
	intIndexOfMatch = strText.indexOf( strTarget );
	}
	 
	// Return the updated string with ALL the target strings
	// replaced out with the new substring.
	return( strText );
}

module.exports = Util;