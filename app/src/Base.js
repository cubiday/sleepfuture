"use strict"

const EventEmitter = require('events').EventEmitter;
const inherits = require('util').inherits;

const Base = function () {}
inherits(Base, EventEmitter);

Base.prototype.info = function () {
    const date = new Date().toISOString();
    const args = Array.from(arguments);
    const prefix = `${date} [${this.constructor.name}]`;
    args.unshift(prefix);
    console.log.apply(console, args);
}

module.exports = Base;
