const tokenABI = require('./SLEEPFUTURE.json').abi;
const vestingABI = require('./VestingContract.json').abi;

module.exports = { 
    tokenABI, vestingABI
};