"use strict";

require('dotenv').config({
    path:
      process.env.NODE_ENV === 'development'
        ? '.env.development'
        : '.env',
  });
  
  const VestingManager = require("./src/VestingManager");
  new VestingManager().start();