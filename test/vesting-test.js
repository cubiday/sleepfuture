
const Web3 = require('web3');
const web3 = new Web3(new Web3.providers.HttpProvider('http://localhost:8545'));
const SLEEPFUTURE = artifacts.require("SLEEPFUTURE");
const VestingContract = artifacts.require("VestingContract");

const TOTAL_SUPPLY = 10**9;
const TOKEN_NAME = 'SLEEPFUTURE';
const TOKEN_SYMBOL = 'SLEEPEE';
const TOKEN_DECIMALs = 18;
const seedAddress = '0x95e2390c53fe9b202bd71ce87BECcCe568b8502F';
const presaleAddress = '0xA1D201ac57dA5692c30F32B0f51677988c1F429E';
const privateSaleAddress = '0x9Dfb2C2F89d77cE2bFb7EabB2F611A6Da6b8f0dc';
const publicsaleAddress = '0x91d7963362caB391Cda66d9a3BbfA95F014607A1';
const teamAddress = '0xEB5754a30E7906Da49B218219944fE31CE3467cE';

const PK_TEST_ACCOUNT = '0xdbffe2527f143b63ca4d1e92e12e624f3363711bdaed0d46833df62927185b9f'; // genesis to test account
const rewardsAddress = '0x1066A4AaBa6f3F4d8e2172090d28f50dbeEc0809';
const foundationAddress = '0x8681970709757c692D999fA2307FfD752fb6BF55';
const marketingAddress = '0x1a69F0f5Dd9fBF578717ceE198aC45690b1E0F49';
const liquidityAddress = '0x245ab458f29453Ee74Bea85f3b08d17a7Cb5ACc5';
const reserveAddress = '0x6ed021fBb8Aaf3cEc159A3Ab44c946dF42dF7fA8';

function timeout(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
}
async function blocktime() {
    let blockNumber = await web3.eth.getBlockNumber();
    let block = await web3.eth.getBlock(blockNumber);
    // console.log('block', block);
    return block['timestamp'] * 1000;
}
async function tryCatch(promise) {
    try {
        await promise;
    }
    catch (error) {
        console.log(error);
        assert(error, "Expected an error but did not get one");
    }
};

contract("SLEEPFUTURE", accounts => {
    // check basic information
    it("Should has name is SLEEPFUTURE", async () => {
        await blocktime();
        let tokenContract = await SLEEPFUTURE.deployed();
        let name = await tokenContract.name();
        assert.equal(name, TOKEN_NAME);
    });

    it("Should has symbol is SLEEPEE", async () => {
        let tokenContract = await SLEEPFUTURE.deployed();
        let symbol = await tokenContract.symbol();
        assert.equal(symbol, TOKEN_SYMBOL);
    });

    it(`Should has decimals is ${TOKEN_DECIMALs}`, async () => {
        let tokenContract = await SLEEPFUTURE.deployed();
        let decimals = await tokenContract.decimals();
        assert.equal(decimals, TOKEN_DECIMALs);
    });
    it("Should has 1 billion token in total", async () => {
        let tokenContract = await SLEEPFUTURE.deployed();
        let totalSupply = await tokenContract.totalSupply();
        assert.equal(totalSupply, web3.utils.toWei(`${TOTAL_SUPPLY}`));

    });
    
    //check genesis distrution
    it(`Seed address ${seedAddress} should has ${TOTAL_SUPPLY*4*5/10000} token `, async () => {
        let tokenContract = await SLEEPFUTURE.deployed();
        let balance = await tokenContract.balanceOf(seedAddress);

        assert.equal(web3.utils.toWei(`${TOTAL_SUPPLY*4*5/10000}`), balance.toString());
    });
    it(`Private Sale address ${privateSaleAddress} should has ${TOTAL_SUPPLY*5*10/10000} token `, async () => {
        let tokenContract = await SLEEPFUTURE.deployed();
        let balance = await tokenContract.balanceOf(privateSaleAddress);

        assert.equal(web3.utils.toWei(`${TOTAL_SUPPLY*5*10/10000}`), balance.toString());
    });
    it(`Presale address ${presaleAddress} should has ${0} token `, async () => {
        let tokenContract = await SLEEPFUTURE.deployed();
        let balance = await tokenContract.balanceOf(presaleAddress);

        assert.equal(web3.utils.toWei(`${0}`), balance.toString());
    });
    it(`Public sale address ${publicsaleAddress} should has ${TOTAL_SUPPLY*1*20/10000} token `, async () => {
        let tokenContract = await SLEEPFUTURE.deployed();
        let balance = await tokenContract.balanceOf(publicsaleAddress);

        assert.equal(web3.utils.toWei(`${TOTAL_SUPPLY*1*20/10000}`), balance.toString());
    });
    it(`Team address ${teamAddress} should has ${0} token `, async () => {
        let tokenContract = await SLEEPFUTURE.deployed();
        let balance = await tokenContract.balanceOf(teamAddress);

        assert.equal(web3.utils.toWei(`${0}`), balance.toString());
    });
    it(`Rewards address ${rewardsAddress} should has ${TOTAL_SUPPLY*30*2/10000} token `, async () => {
        let tokenContract = await SLEEPFUTURE.deployed();
        let balance = await tokenContract.balanceOf(rewardsAddress);

        assert.equal(web3.utils.toWei(`${TOTAL_SUPPLY*30*2/10000}`), balance.toString());
    });
    it(`Foundation address ${foundationAddress} should has ${TOTAL_SUPPLY*20*5/10000} token `, async () => {
        let tokenContract = await SLEEPFUTURE.deployed();
        let balance = await tokenContract.balanceOf(foundationAddress);

        assert.equal(web3.utils.toWei(`${TOTAL_SUPPLY*20*5/10000}`), balance.toString());
    });
    it(`Marketing address ${marketingAddress} should has ${TOTAL_SUPPLY*10*0/10000} token `, async () => {
        let tokenContract = await SLEEPFUTURE.deployed();
        let balance = await tokenContract.balanceOf(marketingAddress);

        assert.equal(web3.utils.toWei(`${TOTAL_SUPPLY*10*0/10000}`), balance.toString());
    });
    it(`Liquidity address ${liquidityAddress} should has ${TOTAL_SUPPLY*3*30/10000} token `, async () => {
        let tokenContract = await SLEEPFUTURE.deployed();
        let balance = await tokenContract.balanceOf(liquidityAddress);

        assert.equal(web3.utils.toWei(`${TOTAL_SUPPLY*3*30/10000}`), balance.toString());
    });
    it(`Reserve address ${reserveAddress} should has ${TOTAL_SUPPLY*3*10/10000} token `, async () => {
        let tokenContract = await SLEEPFUTURE.deployed();
        let balance = await tokenContract.balanceOf(reserveAddress);

        assert.equal(web3.utils.toWei(`${TOTAL_SUPPLY*3*10/10000}`), balance.toString());
    });   

    it("Should not pause as default and able to transfer token", async () => {
        let tokenContract = await SLEEPFUTURE.deployed();
        let isPaused = await tokenContract.paused();
        assert.equal(isPaused,false);

        // transfer token
        let transferAmount = 10000000000000;
        let txHash = await tokenContract.transfer(accounts[1], transferAmount, {from : accounts[0]});
        let balance = await tokenContract.balanceOf(accounts[1]);

        assert.equal(txHash.receipt.status, true);
        assert.equal(balance, transferAmount);
    });

    it("Should able to pause", async () => {
        let tokenContract = await SLEEPFUTURE.deployed();
        let isPaused = await tokenContract.paused();
        assert.equal(isPaused,false);

        await tokenContract.pause();
        isPaused = await tokenContract.paused();
        assert.equal(isPaused, true);
    });

    it("Should able to burn", async () => {
        let tokenContract = await SLEEPFUTURE.deployed();
        await tokenContract.unpause();
        let BN = web3.utils.BN;

        let tokenHolder = await web3.eth.accounts.privateKeyToAccount(PK_TEST_ACCOUNT);
        let transferAmount = 10000000000000;
        let bBefore = await tokenContract.balanceOf(tokenHolder.address);

        await tokenContract.burn(transferAmount, {from: tokenHolder.address});

        let bAfter = await tokenContract.balanceOf(tokenHolder.address);
        bAfter = bAfter.add(new BN(`${transferAmount}`));

        assert.equal(bAfter.toString(), bBefore.toString());
    });

    it("Should has 10 vesting contracts", async () => {
        let tokenContract = await SLEEPFUTURE.deployed();
        let vestings = await tokenContract.getVestings();
        assert.equal(vestings.length, 10);
    });

    // check vesting/cliff information (beneficial/amount/...)
    it(`Checking vesting/cliff for seed`, async() => {
        let tokenContract = await SLEEPFUTURE.deployed();
        let vestings = await tokenContract.getVestings();

        // checking seed vesting info
        let vestingContract = await VestingContract.at(vestings[0]);
        let vestingInfo = await vestingContract.vestingInfo();
        let { _token,_beneficiary, _amount, _distributed, _percentage } = vestingInfo;
        assert.equal(_beneficiary.toLowerCase(), seedAddress.toLowerCase());
        assert.equal(_amount.toString(), web3.utils.toWei(`${4*TOTAL_SUPPLY/100}`));
        assert.equal(_distributed.toString(), web3.utils.toWei(`${4*5*TOTAL_SUPPLY/10000}`));
        assert.equal(_percentage.toString(), 791);
    });
    // check vesting/cliff information (beneficial/amount/...)
    it(`Checking vesting/cliff for private sale`, async() => {
        let tokenContract = await SLEEPFUTURE.deployed();
        let vestings = await tokenContract.getVestings();

        // checking seed vesting info
        let vestingContract = await VestingContract.at(vestings[1]);
        let vestingInfo = await vestingContract.vestingInfo();
        let { _token,_beneficiary, _amount, _distributed, _percentage } = vestingInfo;
        assert.equal(_beneficiary.toLowerCase(), privateSaleAddress.toLowerCase());
        assert.equal(_amount.toString(), web3.utils.toWei(`${5*TOTAL_SUPPLY/100}`));
        assert.equal(_distributed.toString(), web3.utils.toWei(`${5*10*TOTAL_SUPPLY/10000}`));
        assert.equal(_percentage.toString(), 1000);
    });

    it(`Checking vesting/cliff for pre sale`, async() => {
        let tokenContract = await SLEEPFUTURE.deployed();
        let vestings = await tokenContract.getVestings();

        // checking seed vesting info
        let vestingContract = await VestingContract.at(vestings[2]);
        let vestingInfo = await vestingContract.vestingInfo();
        let { _token,_beneficiary, _amount, _distributed, _percentage } = vestingInfo;
        assert.equal(_beneficiary.toLowerCase(), presaleAddress.toLowerCase());
        assert.equal(_amount.toString(), web3.utils.toWei(`${8*TOTAL_SUPPLY/100}`));
        assert.equal(_distributed.toString(), web3.utils.toWei(`${0}`));
        assert.equal(_percentage.toString(), 1677);
    });
    it(`Checking vesting/cliff for public sale`, async() => {
        let tokenContract = await SLEEPFUTURE.deployed();
        let vestings = await tokenContract.getVestings();

        // checking seed vesting info
        let vestingContract = await VestingContract.at(vestings[3]);
        let vestingInfo = await vestingContract.vestingInfo();
        let { _token,_beneficiary, _amount, _distributed, _percentage } = vestingInfo;
        assert.equal(_beneficiary.toLowerCase(), publicsaleAddress.toLowerCase());
        assert.equal(_amount.toString(), web3.utils.toWei(`${1*TOTAL_SUPPLY/100}`));
        assert.equal(_distributed.toString(), web3.utils.toWei(`${1*20*TOTAL_SUPPLY/10000}`));
        assert.equal(_percentage.toString(), 1333);
    });
    it(`Checking vesting/cliff for team`, async() => {
        let tokenContract = await SLEEPFUTURE.deployed();
        let vestings = await tokenContract.getVestings();

        // checking seed vesting info
        let vestingContract = await VestingContract.at(vestings[4]);
        let vestingInfo = await vestingContract.vestingInfo();
        let { _token,_beneficiary, _amount, _distributed, _percentage } = vestingInfo;
        assert.equal(_beneficiary.toLowerCase(), teamAddress.toLowerCase());
        assert.equal(_amount.toString(), web3.utils.toWei(`${16*TOTAL_SUPPLY/100}`));
        assert.equal(_distributed.toString(), web3.utils.toWei(`${0}`));
        assert.equal(_percentage.toString(), 500);
    });
    it(`Checking vesting/cliff for reward`, async() => {
        let tokenContract = await SLEEPFUTURE.deployed();
        let vestings = await tokenContract.getVestings();

        // checking seed vesting info
        let vestingContract = await VestingContract.at(vestings[5]);
        let vestingInfo = await vestingContract.vestingInfo();
        let { _token,_beneficiary, _amount, _distributed, _percentage } = vestingInfo;
        assert.equal(_beneficiary.toLowerCase(), rewardsAddress.toLowerCase());
        assert.equal(_amount.toString(), web3.utils.toWei(`${30*TOTAL_SUPPLY/100}`));
        assert.equal(_distributed.toString(), web3.utils.toWei(`${30*2*TOTAL_SUPPLY/10000}`));
        assert.equal(_percentage.toString(), 450);
    });

    it(`Checking vesting/cliff for foudation`, async() => {
        let tokenContract = await SLEEPFUTURE.deployed();
        let vestings = await tokenContract.getVestings();

        // checking seed vesting info
        let vestingContract = await VestingContract.at(vestings[6]);
        let vestingInfo = await vestingContract.vestingInfo();
        let { _token,_beneficiary, _amount, _distributed, _percentage } = vestingInfo;
        assert.equal(_beneficiary.toLowerCase(), foundationAddress.toLowerCase());
        assert.equal(_amount.toString(), web3.utils.toWei(`${20*TOTAL_SUPPLY/100}`));
        assert.equal(_distributed.toString(), web3.utils.toWei(`${20*5*TOTAL_SUPPLY/10000}`));
        assert.equal(_percentage.toString(), 450);
    });
    it(`Checking vesting/cliff for marketing`, async() => {
        let tokenContract = await SLEEPFUTURE.deployed();
        let vestings = await tokenContract.getVestings();

        // checking seed vesting info
        let vestingContract = await VestingContract.at(vestings[7]);
        let vestingInfo = await vestingContract.vestingInfo();
        let { _token,_beneficiary, _amount, _distributed, _percentage } = vestingInfo;
        assert.equal(_beneficiary.toLowerCase(), marketingAddress.toLowerCase());
        assert.equal(_amount.toString(), web3.utils.toWei(`${10*TOTAL_SUPPLY/100}`));
        assert.equal(_distributed.toString(), web3.utils.toWei(`${0}`));
        assert.equal(_percentage.toString(), 300);
    });
    it(`Checking vesting/cliff for liquidity`, async() => {
        let tokenContract = await SLEEPFUTURE.deployed();
        let vestings = await tokenContract.getVestings();

        // checking seed vesting info
        let vestingContract = await VestingContract.at(vestings[8]);
        let vestingInfo = await vestingContract.vestingInfo();
        let { _token,_beneficiary, _amount, _distributed, _percentage } = vestingInfo;
        assert.equal(_beneficiary.toLowerCase(), liquidityAddress.toLowerCase());
        assert.equal(_amount.toString(), web3.utils.toWei(`${3*TOTAL_SUPPLY/100}`));
        assert.equal(_distributed.toString(), web3.utils.toWei(`${3*30*TOTAL_SUPPLY/10000}`));
        assert.equal(_percentage.toString(), 500);
    });
    it(`Checking vesting/cliff for reserve`, async() => {
        let tokenContract = await SLEEPFUTURE.deployed();
        let vestings = await tokenContract.getVestings();

        // checking seed vesting info
        let vestingContract = await VestingContract.at(vestings[9]);
        let vestingInfo = await vestingContract.vestingInfo();
        let { _token,_beneficiary, _amount, _distributed, _percentage } = vestingInfo;
        assert.equal(_beneficiary.toLowerCase(), reserveAddress.toLowerCase());
        assert.equal(_amount.toString(), web3.utils.toWei(`${3*TOTAL_SUPPLY/100}`));
        assert.equal(_distributed.toString(), web3.utils.toWei(`${3*10*TOTAL_SUPPLY/10000}`));
        assert.equal(_percentage.toString(), 500);
    });
    it(`Release token after cliff time `, async () => {
        let tokenContract = await SLEEPFUTURE.deployed();
        await tokenContract.pause();
        await timeout(3*60*1000);
        await tokenContract.unpause();

        let vestings = await tokenContract.getVestings();

        for(let idx = 0; idx < vestings.length; idx++) {
            let vestingContract = await VestingContract.at(vestings[idx]);
            // console.log();
            // let balance = await tokenContract.balanceOf(vestings[idx]);
            // console.log(vestings[idx], 'Balance', balance.toString());
            await vestingContract.release();
        }
    });
});