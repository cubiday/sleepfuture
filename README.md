# SLEEPFUTURE

## Compile and test on local
Run `truffle compile` and then `npm i`

## Start ganache with command
`ganache -m "food fiber biology budget already lyrics snake super funny goose sport calm"`

## Comment line 29 of SLEEPFUTURE.sol and uncomment line 30
    ```
    constructor () ERC20 ("SLEEPFUTURE", "SLEEPEE"){
        //_genesisDistributeAndLock();
        _genesisDistributeAndLockTestnet();
    }
    ```
## Run truffle test
`truffle test`
